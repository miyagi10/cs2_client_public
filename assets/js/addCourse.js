let courseForm = document.querySelector('#createCourse');

console.log(courseForm);

const token = localStorage.getItem('token');

console.log(token);

courseForm.addEventListener('submit', (e) => {
	e.preventDefault();

	let name = document.querySelector('#courseName').value;
	let price = document.querySelector('#coursePrice').value;
	let description = document.querySelector('#courseDescription').value;

	console.log(name);
	console.log(price);
	console.log(description);

	if (name !== '' && price !== '' && description !== '') {
		fetch('http://localhost:4000/api/course', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${token}`,
			},
			body: JSON.stringify({
				name: name,
				price: price,
				description: description,
			}),
		})
			.then((res) => {
				return res.json();
			})
			.then((data) => {
				console.log(data);
				if (data === true) {
				} else {
					alert('Something went wrong');
				}
				alert('New Course has been Added Seccessfully!');

				window.location.href = './courses.html';
			});
	}
});
