courseId = localStorage.getItem('courseId');
console.log(courseId);

let courseExitBtn = document.querySelector('#courseExitBtn');

let courseName = document.querySelector('#courseName');

let coursePrice = document.querySelector('#coursePrice');

let courseDesc = document.querySelector('#courseDesc');

removeCourseId = () => {
	localStorage.removeItem('courseId', courseId);
	window.location.href = './courses.html';
};

fetch(`http://localhost:4000/api/course/${courseId}`, {
	method: 'GET',
	headers: {
		'Content-Type': 'application/json',
	},
})
	.then((res) => res.json())
	.then((data) => {
		courseName.innerHTML = data.name;
		coursePrice.innerHTML = data.price;
		courseDesc.innerHTML = data.description;
	});

courseExitBtn.addEventListener('click', removeCourseId);
