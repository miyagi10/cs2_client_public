let courseContainer = document.querySelector('#courseContainer');

let addBtn = document.querySelector('#adminButton');

let profileBtn = document.querySelector('#nav-profile');

let logoutBtn = document.querySelector('#logoutBtn');

// let courseId = ' ';

const token = localStorage.getItem('token');
console.log(token);

const isAdmin = localStorage.getItem('isAdmin');
console.log(isAdmin);

fetch('http://localhost:4000/api/course', {
	method: 'GET',
	headers: {
		'Content-Type': 'application/json',
	},
})
	.then((res) => res.json())
	.then((data) => {
		let course = data
			.map((classData) => {
				console.log(classData);
				console.log(classData._id);
				courseId = classData._id;

				return `<tr>
                        <td>${classData.name}</td>
                        <td>${classData.description}</td>
                        <td>${classData.price}</td>
						<td>
						
						<a href="#" class="btn" id="enrollBtn">Enroll Now</a>

						<a href="./course.html" class="btn" id="viewBtn">
						<i class="fas fa-eye"></i>
						</a>

						<a href="#" class="btn" id="editBtn">
						<i class="fas fa-edit"></i>
						</a>

						<a href="#" class="btn" id="deleteBtn">
						<i class="fas fa-trash-alt"></i>
						</a>

                        </td>
					</tr>`;
			})
			.join('');
		courseContainer.innerHTML = `<table class="table">
				<thead>
				<tr>
                <th> Course Name </th>
                <th> Description </th>
                <th> Price </th>
                <th> Action </th>
				</tr>
				<tbody>
                ${course}
				</tbody>
				</thead>
				</table>`;
	})
	.then((data) => {
		let viewBtn = document.querySelector('#viewBtn');
		console.log(viewBtn);

		getCourseId = () => {
			localStorage.setItem('courseId', courseId);
		};
		console.log(courseId);

		viewBtn.addEventListener('click', getCourseId);
	});

let enrollBtn = document.querySelector('#enrollBtn');

let editBtn = document.querySelector('#editBtn');

if (isAdmin == 'false' || token == null) {
	addBtn.innerHTML = '';
}

if (isAdmin == 'false' || token == null) {
	addBtn.innerHTML = '';
}

if (isAdmin == 'true' || token == null) {
	profileBtn.innerHTML = '';
	enrollBtn.innerHTML = '';
}

if (token == null) {
	logoutBtn.innerHTML = '';
}
