// console.log('hello from login');

let loginForm = document.querySelector('#loginUser');

const isAdmin = localStorage.getItem('isAdmin');
console.log(isAdmin);

loginForm.addEventListener('submit', (e) => {
	e.preventDefault();

	let email = document.querySelector('#userEmail').value;

	let password = document.querySelector('#password').value;

	console.log(email);
	console.log(password);

	//let's now create a control structure that will allow us to determine if there is data inserted in the given fields
	if (email == '' || password == '') {
		alert('Please input email and/or password!');
	} else {
		fetch('http://localhost:4000/api/users/login', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
			},
			body: JSON.stringify({
				email: email,
				password: password,
			}),
		})
			.then((res) => {
				return res.json();
			})
			.then((data) => {
				//to check if the data has been caught, let's display it first in the console
				console.log(data);
				//upon successful authentication it will return a JWT
				//lets create a control structure to determine if the JWT has been generated together with the user credentials in an object format
				if (data.access) {
					//we are going to store now the JWT inside our localStorage
					localStorage.setItem('token', data.access);
					localStorage.setItem('id', data.userId);
					localStorage.setItem('isAdmin', data.isAdmin);

					if (isAdmin == 'true') {
						window.location.replace('./courses.html');
						// addBtn.innerHTML = '';
					} else {
						window.location.replace('./profile.html');
					}

					//once that the id is authenticated successfully using the access token, then it should redirect the user  to the user's profile page
				} else {
					//create an else branch that will run if an access key is not found
					alert('Something Went Wrong, Check your Credentials!');
				}
			});
	}
});
