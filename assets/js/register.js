// console.log('hello from register');

let registerForm = document.querySelector('#registerUser');

registerForm.addEventListener('submit', (e) => {
	e.preventDefault();

	let firstName = document.querySelector('#firstName').value;
	let lastName = document.querySelector('#lastName').value;
	let email = document.querySelector('#userEmail').value;
	let mobileNo = document.querySelector('#mobileNumber').value;
	let password1 = document.querySelector('#password1').value;
	let password2 = document.querySelector('#password2').value;

	/* 	console.log(firstName);
	console.log(lastName);
	console.log(email);
	console.log(mobileNo);
	console.log(password1);
    console.log(password2); */

	//let's create a validation to enable the submit button when all fields are registered and if both passwords match
	if (
		password1 !== '' &&
		password2 !== '' &&
		password2 === password1 &&
		mobileNo.length === 11
	) {
		//if all the requirements are met, then now let's check for duplicate emails
		fetch('https://cs2-negosyo-academy.herokuapp.com/api/users/email-exists', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
			},
			body: JSON.stringify({
				//this is a method that converts a javascript object value into a JSON string
				email: email,
			}),
		})
			.then((res) => res.json())
			.then((data) => {
				//we will now creae a control structure to see if there are no ducplicates found
				if (data === false) {
					fetch('https://cs2-negosyo-academy.herokuapp.com/api/users', {
						method: 'POST',
						headers: {
							'Content-Type': 'application/json',
						},
						body: JSON.stringify({
							firstName: firstName,
							lastName: lastName,
							email: email,
							password: password1,
							mobileNo: mobileNo,
						}),
					})
						.then((res) => {
							//res.send unction sets the content type to text/html which means that the client will now treat the response as text. it then returns a response to the client
							return res.json();
						})
						.then((data) => {
							console.log(data);
							if (data === true) {
								alert('New Account has Registered Successfuly');
								window.location.replace('./login.html');
							} else {
								alert('Something went wrong in the registration');
							}
						});
				}
			});
	} else {
		alert('Something went wrong, check your credentials');
	}
});
